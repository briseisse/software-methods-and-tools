Вариант №2105: Пенсионный фонд Российской Федерации. Общие сведения о пенсионном фонде, пресс-центр, региональные новости. Информация о предоставлении и оформлении пенсии, социальных доплатах, перерасчете пенсии, социальных программах и др. Сведения для работодателей - http://www.pfrf.ru

Составить список требований, предъявляемых к разрабатываемому веб-сайту (в соответствии с вариантом). Требования должны делиться на следующие категории:

-Функциональные.
Требования пользователей сайта.
Требования владельцев сайта.

-Нефункциональные.
Требования необходимо оформить в соответствии с шаблонами RUP (документ SRS - Software Requirements Specification). Для каждого из требований нужно указать его атрибуты (в соответствии с методологией RUP), а также оценить и аргументировать приблизительное количество часов, требующихся на реализацию этого требования.

Для функциональных требований нужно составить UML UseCase-диаграммы, описывающие реализующие их прецеденты использования.

Отчёт по лабораторной работе должен содержать:

Документ Software Requirements Specification, содержащий список требований к сайту.
UseCase-диаграммы прецедентов использования, реализующих функциональные требования.
Выводы по работе.
Вопросы к защите лабораторной работы:

Методологии разработки ПО. Унифицированный процесс.
Требования и их категоризация. Атрибуты требований.
Язык UML.
Прецеденты использования. UseCase-диаграммы - состав, виды связей.


---en---

Option No. 2105: Pension Fund of the Russian Federation. General information about the pension fund, press center, regional news. Information about the provision and registration of pensions, social surcharges, recalculation of pensions, social programs, etc. Information for employers - http://www.pfrf.ru

Make a list of requirements for the website being developed (in accordance with the option). The requirements should be divided into the following categories:

-Functional.
Requirements of the site users.
Requirements of the site owners.

-Non-functional.
The requirements must be drawn up in accordance with the RUP templates (SRS - Software Requirements Specification document). For each of the requirements, you need to specify its attributes (in accordance with the RUP methodology), as well as estimate and argue the approximate number of hours required to implement this requirement.

For functional requirements, you need to create UML UseCase diagrams describing the use cases that implement them.

The report on the laboratory work should contain:

A Software Requirements Specification document containing a list of site requirements.
UseCase-diagrams of use cases that implement functional requirements.
Conclusions on the work.


Questions for the protection of laboratory work:

Software development methodologies. Unified process.
Requirements and their categorization. Attributes of requirements.
The UML language.
Use cases. UseCase-diagrams - composition, types of connections.
